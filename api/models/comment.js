const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
    article: { type: mongoose.Schema.Types.ObjectId, ref: 'Article', require: true },
    username: { type: String, require: true },
    comment: { type: String, require: true }
});

module.exports = mongoose.model('Comment', commentSchema);